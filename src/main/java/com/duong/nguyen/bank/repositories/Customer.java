package com.duong.nguyen.bank.repositories;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;


@Data
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "Customer", schema = "public")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    private String fullname;
}
