package com.duong.nguyen.bank.controllers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.duong.nguyen.bank.repositories.Customer;
import com.duong.nguyen.bank.repositories.CustomerRepository;

@RestController
public class CustomerController {

    private final CustomerRepository customerRepository; 
    
    CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @PostMapping("/customer")
    public String newCustoner(@RequestBody String newCustomer) {
        System.out.println(".... testing .....");
        return newCustomer;
    }

    @GetMapping("/customer")
    public String GetCustomer() {
        return "new Customer";
    }

    @GetMapping("/customers")
    public Iterable<Customer> GetAllCustomers() {
        return this
            .customerRepository
            .findAll();
    }
}
