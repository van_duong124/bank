CREATE TABLE IF NOT EXISTS Customer (
    id varchar(100) NOT NULL,
    fullname varchar(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS Account (
    id varchar(100) NOT NULL,
    customer_id varchar(100) NOT NULL,
    balance int NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (customer_id) REFERENCES Customer(id)
);

INSERT INTO Customer (id, fullname)
VALUES
('1', 'Arisha Barron'),
('2', 'Branden Gibson'),
('3', 'Rhonda Church'),
('4', 'Georgina Hazel');

INSERT INTO Account (id, customer_id, balance)
VALUES
('11', '1', 0),
('12', '1', 100),
('21', '2', 10),
('31', '3', 20),
('41', '4', 30);
